package com.sandbox3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sandbox3Application {

    public static void main(String[] args) {
        SpringApplication.run(Sandbox3Application.class, args);
    }

}
